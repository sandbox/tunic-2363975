<?php

/**
 * @file
 * Taxonomy Inherited Field Values main file.
 */


/**
 * Implementation of hook_entity_property_info_alter().
 *
 * @param array $info
 *   Current entity info.
 */
function tifv_entity_property_info_alter(&$info) {

  $properties = &$info['taxonomy_term']['properties'];

  // This porperty indicates which field values are inherited form parent terms.
  $properties['inherited_fields'] = array(
    'label' => t('Inherited field names'),
    'description' => t('Holds the fields names that have been inherited.'),
    'getter callback' => 'entity_property_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
    'type' => 'list<text>',
    'computed' => TRUE,
  );

  // This property indicates that this term has been loaded with inherited
  // field values mechanism. It's possible that no field values have been
  // inherited because original term had all fields with values or no parent
  // had a value for an empty field in original term.
  $properties['original_tid'] = array(
    'label' => t('Original tid'),
    'description' => t('This term the result of the inherited field values added to the original tid term.'),
    'getter callback' => 'entity_property_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
    'type' => 'integer',
    'computed' => TRUE,
  );
}

/** * Loads default values for a a taxonomy term.
 *
 * Walks through the taxonomy tree and gets the values for all fields in the
 * vocabulary. If the term has a value for a field this value is selected. If
 * not, the parent term is processed. If this parent term has value for a field
 * this value is selected. If not, parent term is processed. This cycle goes on
 * until the root vocabulary term is reached or values for all fields are
 * selected.
 *
 * @param int $tid
 *
 * @return EntityMetadataWrapper
 *   Object with the constructed virtual term or NULL if tid is invalid.
 */
function tifv_load_inherited_values($tid) {
  $taxonomy_term = entity_metadata_wrapper('taxonomy_term', $tid);

  if ($taxonomy_term) {
    $fields_info = field_info_instances('taxonomy_term', $taxonomy_term->vocabulary->machine_name->value());
    $modified_term = tifv_get_inherited_values($taxonomy_term, $fields_info);
  }
  tifv_prepare_inherited_term($modified_term);
  return $modified_term;
}


/**
 * Prepares the recursion to get the inherited field values for a term.
 *
 * @param EntityMetadataWrapper $term_wp
 *   Wrapped taxonomy term to get its default field values
 * @param array $fields_info
 *   Array with vocabulary field info of the taxonomy term as returned by the
 *   field_info_intances().
 *
 * @return array
 */
function tifv_get_inherited_values($term_wp, $fields_info) {
  $modified_term = clone $term_wp;
  $field_names = array_keys($fields_info);
  tifv_get_inherited_values_recursive($term_wp, $field_names, $modified_term, LANGUAGE_NONE);
  return $modified_term;
}


/**
 * Recursive function to get the default field values of a taxonomy term.
 *
 * The work is done here!
 *
 * @param EntityMetadataWrapper $term_wp
 *   Wrapped taxonomy term to get its default field values
 * @param array  $field_names
 *   Array with the in process term's vocabulary field names.
 * @param array $values
 *   Array with pairs of field name and defauilt field value. This array
 *   is builded during the recursion process and holds the final result.
 * @param string $lang
 *   Language to use when getting the field values.
 */
function tifv_get_inherited_values_recursive($term_wp, $field_names, $modified_term, $lang = LANGUAGE_NONE) {

  // For every not filled fuield the current term value is assinged.
  // If all values are filled recursion is stopped. If any is empty and
  // a parent term exists a new recursion call is made.
 $continue = FALSE;

 // Iterate over term's vocabulary field names.
  foreach ($field_names as $field_name) {
    // If this field is empty get current term value for this field.
    if ($modified_term->{$field_name}->value() === NULL) {
      $value = $term_wp->{$field_name}->value();
      if ($value !== NULL) {
        tifv_inherit_field_value($term_wp, $modified_term, $field_name);
      }
    }
    else {
      $continue = TRUE;
    }
  }

  if ($continue) {
    // A new recursion is needed, try to find the parent term,
    $parents = taxonomy_get_parents($term_wp->tid->value());

    if (count($parents)) {
      // A new parent is found. It's assumed that only one parent is returned,
      // so the first element for array is extracted.
      $parent_term = reset($parents);
      // Warp term in an Entity Metadata Wrapper.
      $parent_term_emw = entity_metadata_wrapper('taxonomy_term', $parent_term->tid);
      // Make recursion call.
      tifv_get_inherited_values_recursive($parent_term_emw, $field_names, $modified_term, $lang);
    }
  }
}


/**
 * Prepare a virtual term that has been constructed with inherited values.
 *
 * Changes made to the term:
 *   - tid is deleted and saved in the original_tid property.
 *   - inherited_values property is added and set to TRUE.
 *
 * @param EntityMetadataWrapper $term_wp
 *   A wrapped taxonomy term.
 */
function tifv_prepare_inherited_term($term_wp) {
  // Because EnitityMetadataWrapper doesn't allows write tid property the
  // needed changes are done in the entity instance and then wrapped again.
  $term = $term_wp->value();
  $term->original_tid = $term->tid;
  $term->tid = NULL;
  $term_wp = entity_metadata_wrapper('taxonomy_term', $term);
}


/**
 * Adds an inherited field value to a term.
 *
 * @param EntityMetadataWrapper $term_wp
 *   Term to copy de value from.
 * @param EntityMetadataWrapper $modified_term_wp
 *   Term to copy the value to.
 * @param string $field_name
 *   Field name to copy.
 */
function tifv_inherit_field_value($term_wp, $modified_term_wp, $field_name) {
  // Keep track of the inherited fields.
  $modified_term_wp->inherited_fields[] = $field_name;
  // Set the inherited value to the field.
  $modified_term_wp->{$field_name} = $term_wp->{$field_name}->value();
}

